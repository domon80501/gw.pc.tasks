﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GW.PC.Tasks.HostedServices
{
    public abstract class InfiniteBackgroundService : BackgroundService
    {
        protected readonly ILogger logger;
        protected int sleep = 0;
        protected Guid batchId = Guid.Empty;

        public InfiniteBackgroundService(ILogger logger)
        {
            this.logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                sleep = 10;
                batchId = Guid.NewGuid();

                using (logger.BeginScope("Batch Id: {BatchId}", batchId))
                {
                    try
                    {
                        await ExecuteAsyncInternal(stoppingToken);
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                    }
                    finally
                    {
                        await Task.Delay(TimeSpan.FromSeconds(sleep), stoppingToken);
                    }
                }
            }
        }

        protected abstract Task ExecuteAsyncInternal(CancellationToken stoppingToken);

        protected virtual void LogError(Exception ex, string message = null) =>
            logger.LogError(ex, message ?? "Error while executing the task");
    }
}
