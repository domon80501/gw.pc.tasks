﻿using GW.PC.Core;
using GW.PC.Merchant.Gateway.Context;
using GW.PC.Models.Queues;
using GW.PC.Payment;
using GW.PC.Payment.Api.Client;
using GW.PC.Services.EF;
using GW.PC.Services.Queues.EF;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GW.PC.Tasks.HostedServices
{
    public class PaymentQueryService : InfiniteBackgroundService
    {
        private readonly int retryMinute = 5;
        private readonly int retryMaxCount = 6;
        private PaymentQueryRequest request = null;
        private static Dictionary<string, string> merchantData = new Dictionary<string, string>();

        public PaymentQueryService(ILogger<PaymentQueryService> logger) : base(logger) { }

        protected override async Task ExecuteAsyncInternal(CancellationToken stoppingToken)
        {
            var service = new PaymentQueryRequestService();

            request = await service.Pop();
            if (request == null)
            {
                logger.LogDebug("No new payment Query request found. Sleeping for a while...");

                return;
            }

            try
            {
                if (DateTime.Now.ToE8() > request.NextSendingAt)
                {
                    using (logger.BeginScope("Processing: {RequestId}", request.RequestId))
                    {
                        logger.LogInformation("Complete building context: {@QueryContext}", request);

                        var merchantKey = await new MerchantService().GetMerchantKey(request.MerchantUsername);

                        var context = new MerchantQueryRequestContext
                        {
                            MerchantUsername = request.MerchantUsername,
                            PaymentMethod = request.PaymentType == PaymentType.Deposit ? ((int)request.PaymentMethod).ToString() : string.Empty,
                            MerchantOrderNumber = request.MerchantOrderNumber,
                        };
                        context.Sign = VerifyUtilities.AesEncrypt(PaymentUtilities.CreateSignRawText(context, string.Empty, string.Empty), merchantKey);

                        var response = request.PaymentType == PaymentType.Deposit
                            ? await new DepositsClient().Query(context)
                            : await new WithdrawalsClient().Query(context);

                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

                            logger.LogInformation("Query result: {@RequestId} - Content: {errorContent} Details: {Response}", request.RequestId, responseContent, response);

                            var result = JsonConvert.DeserializeObject<MerchantQueryResponseContext>(responseContent);

                            if (result.Status == TransactionStatus.PendingPayment.ToInt32String() || result.Status == TransactionStatus.UnKnow.ToInt32String())
                            {
                                request.SendCount++;
                                request.NextSendingAt = DateTime.Now.ToE8().AddMinutes(retryMinute);
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            var errorContent = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());
                            logger.LogWarning("Query result failed: {@RequestId} - Content: {errorContent} Details: {Response}", request.RequestId, errorContent, response);
                        }

                        if (request.SendCount < retryMaxCount)
                        {
                            await service.Add(request);
                        }
                    }
                }
                else
                {
                    await service.Add(request);
                }
            }
            catch (Exception ex)
            {
                request.NextSendingAt = DateTime.Now.ToE8().AddMinutes(retryMinute);

                var target = await service.Add(request);

                logger.LogError("Query result error: {target} Details:{Response}", target, ex.ToString());
            }

            sleep = 0;
        }
    }
}
