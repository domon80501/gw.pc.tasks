﻿using GW.PC.Core;
using GW.PC.Models.Queues;
using GW.PC.Payment.Api.Client;
using GW.PC.Services.Queues.EF;
using GW.PC.Web.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GW.PC.Tasks.HostedServices
{
    public class PaymentCallbackService : InfiniteBackgroundService
    {
        private PaymentCallbackRequest request = null;

        public PaymentCallbackService(ILogger<PaymentCallbackService> logger) : base(logger) { }

        protected override async Task ExecuteAsyncInternal(CancellationToken stoppingToken)
        {
            var service = new PaymentCallbackRequestService();

            request = await service.Pop();

            try
            {
                if (request == null)
                {
                    logger.LogDebug("No new payment callback request found. Sleeping for a while...");

                    return;
                }

                using (logger.BeginScope("Processing: {RequestId}", request.RequestId))
                {
                    logger.LogInformation("Complete building context: {@CallbackContext}", request);

                    var context = new PaymentCallbackRequestContext
                    {
                        RequestId = Guid.Parse(request.RequestId),
                        PaymentChannelId = request.PaymentChannelId,
                        PaymentType = request.PaymentType.ToString(),
                        PaymentMethod = request.PaymentMethod.ToString(),
                        Content = request.Content
                    };

                    var response = request.PaymentType == PaymentType.Deposit
                        ? await new DepositCallbacksClient().Callback(context)
                        : await new WithdrawalCallbacksClient().Callback(context);



                    logger.LogInformation("Callback result: {@Response}", response);

                    if (!response.Succeeded)
                    {
                        logger.LogWarning("Callback result failed: {Response}", response);

                        await service.Add(request);
                    }
                }
            }
            catch (Exception ex)
            {
                var target = await service.Add(request);

                logger.LogError("Callback result error: {target} Details:{Response}", target, ex.ToString());
            }


            sleep = 0;
        }
    }
}
