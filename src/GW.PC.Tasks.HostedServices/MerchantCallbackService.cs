﻿using GW.PC.Core;
using GW.PC.Merchant.Gateway.Callback.Api.Client;
using GW.PC.Merchant.Gateway.Callback.Context;
using GW.PC.Models.Queues;
using GW.PC.Payment;
using GW.PC.Payment.Api.Client;
using GW.PC.Services.EF;
using GW.PC.Services.EF.Payment;
using GW.PC.Services.Queues.EF;
using GW.PC.Web.Core;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GW.PC.Tasks.HostedServices
{
    public class MerchantCallbackService : InfiniteBackgroundService
    {
        private static readonly int retryMinute = 5;
        private static readonly int retryMaxCount = 6;
        private MerchantCallbackRequest request = null;
        private static Dictionary<string, string> merchantData = new Dictionary<string, string>();
        public MerchantCallbackService(ILogger<MerchantCallbackService> logger) : base(logger) { }

        protected override async Task ExecuteAsyncInternal(CancellationToken stoppingToken)
        {
            var service = new MerchantCallbackRequestService();

            request = await service.Pop();
            if (request == null)
            {
                logger.LogDebug("No new merchant callback request found. Sleeping for a while...");

                return;
            }

            try
            {
                if (DateTime.Now.ToE8() > request.NextSendingAt)
                {
                    using (logger.BeginScope("Processing: {RequestId}", request.RequestId))
                    {
                        logger.LogInformation("Complete building context: {@CallbackContext}", request);

                        var merchantKey = string.Empty;

                        if (merchantData.ContainsKey(request.MerchantUsername))
                        {
                            merchantKey = merchantData[request.MerchantUsername];
                        }
                        else
                        {
                            merchantKey = await new MerchantService().GetMerchantKey(request.MerchantUsername);

                            merchantData.Add(request.MerchantUsername, merchantKey);
                        }

                        var context = new MerchantCallbackRequestContext
                        {
                            MerchantUsername = request.MerchantUsername,
                            MerchantOrderNumber = request.MerchantOrderNumber,
                            PaymentCenterOrderNumber = request.PaymentCenterOrderNumber,
                            PaymentOrderNumber = request.PaymentOrderNumber,
                            PaymentType = ((int)request.PaymentType).ToString(),
                            PaymentMethod = ((int)request.PaymentMethod).ToString(),
                            RequestedAmount = request.RequestedAmount,
                            ActualAmount = request.ActualAmount,
                            Status = ((int)request.Status).ToString(),
                        };
                        context.Sign = VerifyUtilities.AesEncrypt(PaymentUtilities.CreateSignRawText(context, string.Empty, string.Empty), merchantKey);
                        
                        var gatewayContext = new MerchantCallbackGatewayRequestContext
                        {
                            Content = context,
                            Url = request.MerchantCallbackUrl
                        };

                        var response = await new MerchantCallbackGatewayClient().Request(gatewayContext);

                        if (response.IndexOf("success", StringComparison.OrdinalIgnoreCase) > -1)
                        {
                            logger.LogInformation("Merchant callback result succeed: {@RequestId} - {@Response}", request.RequestId, request);

                            if (request.PaymentType == PaymentType.Withdrawal)
                            {
                                await new MerchantWithdrawalService().UpdateCallbackStatus(request.PaymentCenterOrderNumber);
                            }
                            else
                            {
                                await DepositServiceHelper.UpdateCallbackStatus(request.PaymentMethod, request.PaymentCenterOrderNumber);
                            }

                            return;
                        }
                        else
                        {
                            request.SendCount++;
                            request.NextSendingAt = DateTime.Now.ToE8().AddMinutes(retryMinute);

                            logger.LogWarning("Merchant callback result failed: {@RequestId} - {@Response}", request.RequestId, response);

                            merchantData.Remove(request.MerchantUsername);
                        }

                        if (request.SendCount < retryMaxCount)
                        {
                            await service.Add(request);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                request.NextSendingAt = DateTime.Now.ToE8().AddMinutes(retryMinute);

                var target = await service.Add(request);

                logger.LogError("Merchant callback result error: {target} Details:{@Response}", target, ex.ToString());
            }

            sleep = 0;
        }
    }
}
