﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using GW.PC.Core;

namespace GW.PC.Tasks.HostedServices
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("At least input 1 command line argument, i.e. the environment. Press any key to exit.");

                Console.ReadKey();

                return;
            }

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddCommandLine(args)
                .Build();
            Log.Logger = new LoggerConfiguration()
                .Enrich.WithProperty("SourceContext", "PaymentCenter HostedServices")
                .Enrich.WithProperty("Environment", config["environment"])
                .Enrich.FromLogContext()
                .MinimumLevel.Debug()
                .ReadFrom.Configuration(config)
                .WriteTo.Console(outputTemplate: "[{Timestamp:yyyy-MM-dd HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
                .WriteTo.File("..\\logs\\serilog.txt", outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Properties:j} {Message:lj}{NewLine}{Exception}", rollingInterval: RollingInterval.Day)
                .WriteTo.Seq("http://203.60.2.122:5341", restrictedToMinimumLevel: LogEventLevel.Information)
                .CreateLogger();
            SerilogLogger.Configure(() => Log.Logger);

            try
            {
                Log.Information("Services starting ...");

                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Log.Fatal(ex, "Fatal exception");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseShutdownTimeout(TimeSpan.FromSeconds(15))
                .UseSerilog()
                .UseStartup<Startup>();
    }
}
